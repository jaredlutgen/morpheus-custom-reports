package com.morpheusdata.reports

import com.morpheusdata.core.AbstractReportProvider
import com.morpheusdata.core.MorpheusContext
import com.morpheusdata.core.Plugin
import com.morpheusdata.model.OptionType
import com.morpheusdata.model.ReportResult
import com.morpheusdata.model.ReportType
import com.morpheusdata.model.ReportResultRow
import com.morpheusdata.model.ContentSecurityPolicy
import com.morpheusdata.views.HTMLResponse
import com.morpheusdata.views.ViewModel
import com.morpheusdata.response.ServiceResponse
import com.morpheusdata.util.MorpheusUtils
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.reactivex.Observable
import java.time.LocalDate
import java.time.Period

import java.sql.Connection

/**
 * A set of reports for CSMs to determine more about their customers. .
 * The goal of this report is to help understand how a customer is using the morpheus platform.
 *
 * A renderer is also defined to render the HTML via Handlebars templates.
 *
 * @author Jared Lutgen
 */
 @Slf4j
class CsmDetailedOptionLists extends AbstractReportProvider {
    Plugin plugin
    MorpheusContext morpheusContext

    CsmDetailedOptionLists(Plugin plugin, MorpheusContext context) {
        this.plugin = plugin
        this.morpheusContext = context
    }

    @Override
    MorpheusContext getMorpheus() {
        morpheusContext
    }

    @Override
    Plugin getPlugin() {
        plugin
    }

    @Override
    String getCode() {
        'csm-detailed-optionlist'
    }

    @Override
    String getName() {
        'CSM Detailed Option Lists'
    }

    ServiceResponse validateOptions(Map opts) {
         return ServiceResponse.success()
    }

    /*
     * Demonstrates building a TaskConfig to get details about the Instance and renders the html from the specified template.
     * @param instance details of an Instance
     * @return
     */

    // TODO craete a rendering for the detailed option types 
    @Override
    HTMLResponse renderTemplate(ReportResult reportResult, Map<String, List<ReportResultRow>> reportRowsBySection) {
        ViewModel<String> model = new ViewModel<String>()
        model.object = reportRowsBySection
        getRenderer().renderTemplate("hbs/csmDetailedOptionLists", model)
    }

    /**
     * Allows various sources used in the template to be loaded
     * @return
     */
    @Override
    ContentSecurityPolicy getContentSecurityPolicy() {
        def csp = new ContentSecurityPolicy()
        csp.scriptSrc = '*.jsdelivr.net'
        csp.frameSrc = '*.digitalocean.com'
        csp.imgSrc = '*.wikimedia.org'
        csp.styleSrc = 'https: *.bootstrapcdn.com'
        csp
    }


    void process(ReportResult reportResult) {
        morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.generating).blockingGet();
        Long displayOrder = 0
        String nameInReport = "null"
        List<GroovyRowResult> results = []
        withDbConnection { Connection dbConnection -> results = new Sql(dbConnection).rows("""SELECT 
                                                                                                    option_type_list.name as name,
                                                                                                    option_type_list.source_method as sourceMethod,
                                                                                                    option_type_list.initial_dataset as initialDataset,
                                                                                                    option_type_list.translation_script as translationScript,
                                                                                                    option_type_list.request_script as requestScript,
                                                                                                    option_type_list.ignoresslerrors,
                                                                                                    option_type_list.type as type,
                                                                                                    option_type_list.api_type as apiType,
                                                                                                    (select account.name from account where account.id = option_type_list.account_id) as tenant,
                                                                                                    option_type_list.real_time as realTime
                                                                                                FROM option_type_list
                                                                                                left join account on option_type_list.account_id = account.id
                                                                                        """)}
        log.debug("Results: ${results}")
        Observable<GroovyRowResult> observable = Observable.fromIterable(results) as Observable<GroovyRowResult>
        observable.map{ resultRow ->
            if (reportResult.configMap?.secData){
                nameInReport = MorpheusUtils.randomizer() // randomizer()
            } else{
                nameInReport = resultRow.name
            }
            log.debug("Mapping resultRow ${resultRow}")
            Map<String,Object> data = [name: nameInReport, type: resultRow.type, sourceMethod: resultRow.sourceMethod, initialDataset: resultRow.initialDataset, translationScript: resultRow.translationScript, requestScript: resultRow.requestScript, tenant: resultRow.tenant ]
            ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_MAIN, displayOrder: displayOrder++, dataMap: data)
            log.debug("resultRowRecord: ${resultRowRecord.dump()}")
            return resultRowRecord
        }.buffer(50).doOnComplete {
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.ready).blockingGet();
        }.doOnError { Throwable t ->
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.failed).blockingGet();
        }.subscribe {resultRows ->
            morpheus.report.appendResultRows(reportResult,resultRows).blockingGet()
        }

    }

     @Override
     String getDescription() {
         return "Collection of Reports to understand usage."
     }

     @Override
     String getCategory() {
         return 'Morpheus Customer Success'
     }

     @Override
     Boolean getOwnerOnly() {
         return false
     }

     @Override
     Boolean getMasterOnly() {
         return true
     }

     @Override
     Boolean getSupportsAllZoneTypes() {
         return true
     }
    // TODO create filters for tenants / accounts. 
     @Override
     List<OptionType> getOptionTypes() {
        //  [new OptionType(code: 'secure-data', name: 'Secure', fieldName: 'secData', inputType: OptionType.InputType.CHECKBOX, defaultValue: 0,fieldContext: 'config', fieldLabel: 'Secure data', helpText: 'If you would like to scramble sensitive data please check this box.',displayOrder: 0)]
        //  // [new OptionType(code: 'name', name: 'Search', fieldName: 'phrase', fieldContext: 'config', fieldLabel: 'Name Filter',     helpText: 'If no name supplied, this will search all.', displayOrder: 1),
         //new OptionType(code: 'site', name: 'Search', fieldName: 'group', fieldContext: 'config', fieldLabel: 'Group ID Filter',     helpText: 'If no group ID is supplied, this will search all.', displayOrder: 0)]
     }


}
