package com.morpheusdata.reports

import com.morpheusdata.core.Plugin
import com.morpheusdata.model.Permission

class ReportsPlugin extends Plugin {

	@Override
	String getCode() {
		return 'csm-report'
	}

	@Override
	void initialize() {
		// CsmReports csmReportProvider = new CsmReports(this, morpheus)
		// this.pluginProviders.put(csmReportProvider.code, csmReportProvider)
		// CsmReportsAdv expcustomReportProvider = new CsmReportsAdv(this, morpheus)
		// this.pluginProviders.put(expcustomReportProvider.code, expcustomReportProvider)
		FeatureUsage featureUsage = new FeatureUsage(this, morpheus)
		this.pluginProviders.put(featureUsage.code, featureUsage)
		CsmUserUsage csmUserUsage = new CsmUserUsage(this, morpheus)
		this.pluginProviders.put(csmUserUsage.code, csmUserUsage)
		CsmDetailedOptionTypes csmDetailedOptionTypes = new CsmDetailedOptionTypes(this, morpheus)
		this.pluginProviders.put(csmDetailedOptionTypes.code, csmDetailedOptionTypes)
		CsmDetailedOptionLists csmDetailedOptionLists = new CsmDetailedOptionLists(this, morpheus)
		this.pluginProviders.put(csmDetailedOptionLists.code, csmDetailedOptionLists)
		CsmDetailedClouds csmDetailedClouds = new CsmDetailedClouds(this, morpheus)
		this.pluginProviders.put(csmDetailedClouds.code, csmDetailedClouds)
		this.setName("JL MD Customer Success Reports")
		this.setDescription("JL MD Customer Success Reports")
		
	}

	@Override
	void onDestroy() {
	}
	// @Override
	// public List<Permission> getPermissions() {
	// 	// Define the available permissions for the report
	// 	Permission permission = new Permission('CSM Report Plugin', 'csmReportPlugin', [Permission.AccessType.none, Permission.AccessType.full])
	// 	return [permission];
	// }

}