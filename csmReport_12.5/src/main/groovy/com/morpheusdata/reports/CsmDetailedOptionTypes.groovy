package com.morpheusdata.reports

import com.morpheusdata.core.AbstractReportProvider
import com.morpheusdata.core.MorpheusContext
import com.morpheusdata.core.Plugin
import com.morpheusdata.model.OptionType
import com.morpheusdata.model.ReportResult
import com.morpheusdata.model.ReportType
import com.morpheusdata.model.ReportResultRow
import com.morpheusdata.model.ContentSecurityPolicy
import com.morpheusdata.views.HTMLResponse
import com.morpheusdata.views.ViewModel
import com.morpheusdata.util.MorpheusUtils
import com.morpheusdata.response.ServiceResponse
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.reactivex.Observable

import java.time.LocalDate
import java.time.Period

import java.sql.Connection

/**
 * A set of reports for CSMs to determine more about their customers. .
 * The goal of this report is to help understand how a customer is using the morpheus platform.
 *
 * A renderer is also defined to render the HTML via Handlebars templates.
 *
 * @author Jared Lutgen
 */
 @Slf4j
class CsmDetailedOptionTypes extends AbstractReportProvider {
    Plugin plugin
    MorpheusContext morpheusContext

    CsmDetailedOptionTypes(Plugin plugin, MorpheusContext context) {
        this.plugin = plugin
        this.morpheusContext = context
    }

    @Override
    MorpheusContext getMorpheus() {
        morpheusContext
    }

    @Override
    Plugin getPlugin() {
        plugin
    }

    @Override
    String getCode() {
        'csm-detailed-optiontype'
    }

    @Override
    String getName() {
        'CSM Detailed Option Types'
    }

    ServiceResponse validateOptions(Map opts) {
         return ServiceResponse.success()
    }

    /*
     * Demonstrates building a TaskConfig to get details about the Instance and renders the html from the specified template.
     * @param instance details of an Instance
     * @return
     */

    // TODO craete a rendering for the detailed option types 
    @Override
    HTMLResponse renderTemplate(ReportResult reportResult, Map<String, List<ReportResultRow>> reportRowsBySection) {
        ViewModel<String> model = new ViewModel<String>()
        model.object = reportRowsBySection
        getRenderer().renderTemplate("hbs/csmDetailedOptionTypes", model)
    }

    /**
     * Allows various sources used in the template to be loaded
     * @return
     */
    @Override
    ContentSecurityPolicy getContentSecurityPolicy() {
        def csp = new ContentSecurityPolicy()
        csp.scriptSrc = '*.jsdelivr.net'
        csp.frameSrc = '*.digitalocean.com'
        csp.imgSrc = '*.wikimedia.org'
        csp.styleSrc = 'https: *.bootstrapcdn.com'
        csp
    }


    void process(ReportResult reportResult) {
        morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.generating).blockingGet();
        Long displayOrder = 0
        String nameInReport = "null"
        List<GroovyRowResult> results = []
        withDbConnection { Connection dbConnection -> results = new Sql(dbConnection).rows("""SELECT 
                                                                                                option_type.name as name, 
                                                                                                option_type.type as type, 
                                                                                                option_type.enabled as status,
                                                                                                (select option_type_list.name from option_type_list where option_type_list.id = option_type.option_list_id) as sourceOptionList,
                                                                                                option_type.option_source as optionSource,
                                                                                                (select account.name from account where account.id = option_type.account_id) as tenant
                                                                                            FROM option_type
                                                                                            left join option_type_list on option_type.option_list_id = option_type_list.id
                                                                                            left join account on option_type.account_id = account.id
                                                                                                where field_context = 'config.customOptions'

                                                                                        """)}
        log.debug("Results: ${results}")
        Observable<GroovyRowResult> observable = Observable.fromIterable(results) as Observable<GroovyRowResult>
        observable.map{ resultRow ->
            if (reportResult.configMap?.secData){
                nameInReport = MorpheusUtils.randomizer() // randomizer()
            } else{
                nameInReport = resultRow.name
            }
            //String uniqueCloudTypes = resultRow.uniqueCloudTypes
            log.debug("Mapping resultRow ${resultRow}")
            Map<String,Object> data = [name: nameInReport, type: resultRow.type, status: resultRow.status, sourceOptionList: resultRow.sourceOptionList, optionSource: resultRow.optionSource, tenant: resultRow.tenant ]
            ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_MAIN, displayOrder: displayOrder++, dataMap: data)
            log.debug("resultRowRecord: ${resultRowRecord.dump()}")
            return resultRowRecord
        }.buffer(50).doOnComplete {
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.ready).blockingGet();
        }.doOnError { Throwable t ->
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.failed).blockingGet();
        }.subscribe {resultRows ->
            morpheus.report.appendResultRows(reportResult,resultRows).blockingGet()
        }

    }

     @Override
     String getDescription() {
         return "Collection of Reports to understand usage."
     }

     @Override
     String getCategory() {
         return 'Morpheus Customer Success'
     }

     @Override
     Boolean getOwnerOnly() {
         return false
     }

     @Override
     Boolean getMasterOnly() {
         return true
     }

     @Override
     Boolean getSupportsAllZoneTypes() {
         return true
     }
    // TODO create filters for tenants / accounts. 
     @Override
     List<OptionType> getOptionTypes() {
        //  [new OptionType(code: 'secure-data', name: 'Secure', fieldName: 'secData', inputType: OptionType.InputType.CHECKBOX, defaultValue: 0,fieldContext: 'config', fieldLabel: 'Secure data', helpText: 'If you would like to scramble sensitive data please check this box.',displayOrder: 0)]
        //  // [new OptionType(code: 'name', name: 'Search', fieldName: 'phrase', fieldContext: 'config', fieldLabel: 'Name Filter',     helpText: 'If no name supplied, this will search all.', displayOrder: 1),
         //new OptionType(code: 'site', name: 'Search', fieldName: 'group', fieldContext: 'config', fieldLabel: 'Group ID Filter',     helpText: 'If no group ID is supplied, this will search all.', displayOrder: 0)]
     }
}
