package com.morpheusdata.util

import groovy.util.logging.Slf4j

import java.util.Random

@Slf4j
class MorpheusUtils {
    static randomizer(){
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int length = 10;
        for(int i = 0; i < length; i++) {
        int index = random.nextInt(alphaNumeric.length());
        char randomChar = alphaNumeric.charAt(index);
        sb.append(randomChar);
        }
        String randomString = sb.toString();
        return randomString
    }
}