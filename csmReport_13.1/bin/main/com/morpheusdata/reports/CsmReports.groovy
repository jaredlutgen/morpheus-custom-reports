package com.morpheusdata.reports

import com.morpheusdata.core.AbstractReportProvider
import com.morpheusdata.core.MorpheusContext
import com.morpheusdata.core.Plugin
import com.morpheusdata.model.OptionType
import com.morpheusdata.model.ReportResult
import com.morpheusdata.model.ReportType
import com.morpheusdata.model.ReportResultRow
import com.morpheusdata.model.ContentSecurityPolicy
import com.morpheusdata.views.HTMLResponse
import com.morpheusdata.views.ViewModel
import com.morpheusdata.response.ServiceResponse
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.reactivex.Observable
import java.time.LocalDate
import java.time.Period

import java.sql.Connection

/**
 * A set of reports for CSMs to determine more about their customers. .
 * The goal of this report is to help understand how a customer is using the morpheus platform.
 *
 * A renderer is also defined to render the HTML via Handlebars templates.
 *
 * @author Peter Georgopoulos and Jared Lutgen
 */
 @Slf4j
class CsmReports extends AbstractReportProvider {
    Plugin plugin
    MorpheusContext morpheusContext

    CsmReports(Plugin plugin, MorpheusContext context) {
        this.plugin = plugin
        this.morpheusContext = context
    }

    @Override
    MorpheusContext getMorpheus() {
        morpheusContext
    }

    @Override
    Plugin getPlugin() {
        plugin
    }

    @Override
    String getCode() {
        'csm-report'
    }

    @Override
    String getName() {
        'Customer Success Report'
    }

    ServiceResponse validateOptions(Map opts) {
         return ServiceResponse.success()
    }

    /*
     * Demonstrates building a TaskConfig to get details about the Instance and renders the html from the specified template.
     * @param instance details of an Instance
     * @return
     */
    @Override
    HTMLResponse renderTemplate(ReportResult reportResult, Map<String, List<ReportResultRow>> reportRowsBySection) {
        ViewModel<String> model = new ViewModel<String>()
        model.object = reportRowsBySection
        getRenderer().renderTemplate("hbs/hotshotsReportCard", model)
    }

    /**
     * Allows various sources used in the template to be loaded
     * @return
     */
    @Override
    ContentSecurityPolicy getContentSecurityPolicy() {
        def csp = new ContentSecurityPolicy()
        csp.scriptSrc = '*.jsdelivr.net'
        csp.frameSrc = '*.digitalocean.com'
        csp.imgSrc = '*.wikimedia.org'
        csp.styleSrc = 'https: *.bootstrapcdn.com'
        csp
    }


    void process(ReportResult reportResult) {
        morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.generating).blockingGet();
        Long displayOrder = 0
        List<GroovyRowResult> results = []
        // Working in mysql - but doesn't print the proper information.  
        //withDbConnection { Connection dbConnection -> results = new Sql(dbConnection).rows("SELECT (select GROUP_CONCAT(distinct compute_zone.code) from compute_zone) as uniqueCloudTypes, (select count(compute_zone.name) from compute_zone) as cloudCount, (select count(distinct compute_zone.code) from compute_zone) as uniqueCloudType")}
        withDbConnection { Connection dbConnection -> results = new Sql(dbConnection).rows("SELECT (select GROUP_CONCAT(distinct compute_zone.code) from compute_zone) as uniqueCloudTypes, (select count(compute_zone.name) from compute_zone) as cloudCount, (select count(distinct compute_zone.code) from compute_zone) as uniqueCloudType")}
        log.info("Results: ${results}")
        Observable<GroovyRowResult> observable = Observable.fromIterable(results) as Observable<GroovyRowResult>
        observable.map{ resultRow ->
            String uniqueCloudTypes = resultRow.uniqueCloudTypes
            log.info("Mapping resultRow ${resultRow}")
            Map<String,Object> data = [uniqueCloudType: uniqueCloudTypes, cloudCount: resultRow.cloudCount, uniqueCloudType: resultRow.uniqueCloudType ]
            ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_MAIN, displayOrder: displayOrder++, dataMap: data)
            log.info("resultRowRecord: ${resultRowRecord.dump()}")
            return resultRowRecord
        }.buffer(50).doOnComplete {
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.ready).blockingGet();
        }.doOnError { Throwable t ->
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.failed).blockingGet();
        }.subscribe {resultRows ->
            morpheus.report.appendResultRows(reportResult,resultRows).blockingGet()
        }

    }

     @Override
     String getDescription() {
         return "CSM Report Determine how a customer is using an environment."
     }

     @Override
     String getCategory() {
         return 'CSM'
     }

     @Override
     Boolean getOwnerOnly() {
         return false
     }

     @Override
     Boolean getMasterOnly() {
         return true
     }

     @Override
     Boolean getSupportsAllZoneTypes() {
         return true
     }
    // OPTION TO PUT IN LATER - CALENDAR TO ALLOW THEM TO SELECT A DATE RANGE.
     @Override
     List<OptionType> getOptionTypes() {
         []
         // [new OptionType(code: 'name', name: 'Search', fieldName: 'phrase', fieldContext: 'config', fieldLabel: 'Name Filter',     helpText: 'If no name supplied, this will search all.', displayOrder: 1),
         //new OptionType(code: 'site', name: 'Search', fieldName: 'group', fieldContext: 'config', fieldLabel: 'Group ID Filter',     helpText: 'If no group ID is supplied, this will search all.', displayOrder: 0)]
     }
    
}
