package com.morpheusdata.reports

import com.morpheusdata.core.AbstractReportProvider
import com.morpheusdata.core.MorpheusContext
import com.morpheusdata.core.Plugin
import com.morpheusdata.model.OptionType
import com.morpheusdata.model.ReportResult
import com.morpheusdata.model.ReportType
import com.morpheusdata.model.ReportResultRow
import com.morpheusdata.model.ContentSecurityPolicy
import com.morpheusdata.views.HTMLResponse
import com.morpheusdata.views.ViewModel
import com.morpheusdata.response.ServiceResponse
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.reactivex.Observable;
import java.util.Date
import java.util.concurrent.TimeUnit
import groovy.json.*

import java.sql.Connection

/**
 * A set of reports for CSMs to determine more about their customers. .
 * The goal of this report is to help understand how a customer is using the morpheus platform.
 *
 * A renderer is also defined to render the HTML via Handlebars templates.
 *
 * @author Peter Georgopoulos and Jared Lutgen
 */
 @Slf4j
class CsmReportsAdv extends AbstractReportProvider {
    Plugin plugin
    MorpheusContext morpheusContext

    CsmReportsAdv(Plugin plugin, MorpheusContext context) {
        this.plugin = plugin
        this.morpheusContext = context
    }

    @Override
    MorpheusContext getMorpheus() {
        morpheusContext
    }

    @Override
    Plugin getPlugin() {
        plugin
    }

    @Override
    String getCode() {
        'test'
    }

    @Override
    String getName() {
        'Testing'
    }

    ServiceResponse validateOptions(Map opts) {
         return ServiceResponse.success()
    }

    @Override
    HTMLResponse renderTemplate(ReportResult reportResult, Map<String, List<ReportResultRow>> reportRowsBySection) {
		ViewModel<String> model = new ViewModel<String>()
		def HashMap<String, String> reportPayload = new HashMap<String, String>();
		def webnonce = morpheus.getWebRequest().getNonceToken()
		reportPayload.put("webnonce",webnonce)
		reportPayload.put("reportdata",reportRowsBySection)
		model.object = reportPayload
		getRenderer().renderTemplate("hbs/OverviewReport", model)
    }

    @Override
    ContentSecurityPolicy getContentSecurityPolicy() {
        def csp = new ContentSecurityPolicy()
        csp.scriptSrc = '*.jsdelivr.net'
        csp.frameSrc = '*.digitalocean.com'
        csp.imgSrc = '*.wikimedia.org'
        csp.styleSrc = 'https: *.bootstrapcdn.com'
        csp
    }


    void process(ReportResult reportResult) {
        morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.generating).blockingGet();
        Long displayOrder = 0
        List<GroovyRowResult> results = []
        List<GroovyRowResult> clouds = []
        List<GroovyRowResult> cloudTypes = []
        Connection dbConnection
        Long totalClouds = 0
        Long awsClouds = 0
        Long openstackClouds = 0
        Long standardClouds = 0
        Long otherClouds = 0
       // String taskType
        String cloudType
        def emptyMap = [:]

        try {
            // Create Database Connection
            dbConnection = morpheus.report.getReadOnlyDatabaseConnection().blockingGet()
            results = new Sql(dbConnection).rows("SELECT id,name,code,zone_type_id from compute_zone order by id asc;")
            cloudTypes = new Sql(dbConnection).rows("select id,name,code from compute_zone_type order by id asc;")
        } finally {
            morpheus.report.releaseDatabaseConnection(dbConnection)
        }   
        Observable<GroovyRowResult> observable = Observable.fromIterable(results) as Observable<GroovyRowResult>
        observable.map{ resultRow ->
            log.info("Mapping resultRow ${resultRow}")
            println("Mapping resultRow ${resultRow}")
            totalClouds++

            Boolean isNotFoundTask

            def cloudId = resultRow.zone_type_id
            cloudTypes.each {
                if (cloudId == it.id){
                    cloudType = it.name
                }
            }

            switch(cloudType) {
                case "Amazon":
                    awsClouds++
                    break;
                case "OpenStack":
                    openstackClouds++
                    break;
                case "Morpheus":
                    standardClouds++
                    break;
                default:
                    otherClouds++
            }

        Map<String,Object> data = [name: resultRow.name]
        ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_MAIN, displayOrder: displayOrder++, dataMap: data)
        log.info("resultRowRecord: ${resultRowRecord.dump()}")
        return resultRowRecord
        }.buffer(50).doOnComplete {
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.ready).blockingGet();
        }.doOnError { Throwable t ->
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.failed).blockingGet();
        }.subscribe {resultRows ->
            morpheus.report.appendResultRows(reportResult, resultRows).blockingGet()
        }
        // Ask martez if orderedTasks is a java aspect? 
        Map<String,Object> orderedTasks  = ["Amazon": awsClouds,"OpenStack": openstackClouds, "Morpheus":standardClouds]
        Map<String,Object> maporder = orderedTasks.sort {-it.value}
        def Map<String,Object> topFiveTasks = [:]

        def outdemo = []
        maporder.eachWithIndex {key,val,index ->
            if (index < 5){
                topFiveTasks.put(key, val)
            }
                def g = [:]
                g["name"] = key
                g["count"] = val
                switch(key) {
                    case "Amazon":
                        g["imagesrc"] = "https://commons.wikimedia.org/wiki/File:Amazon_Web_Services_Logo.svg"
                        break;
                    case "OpenStack":
                        g["imagesrc"] = "https://commons.wikimedia.org/wiki/File:Amazon_Web_Services_Logo.svg"
                        break;
                    case "Morpheus":
                        g["imagesrc"] = "https://commons.wikimedia.org/wiki/File:Amazon_Web_Services_Logo.svg"
                        break;
                    default:
                        break;
                }
            outdemo.add(g)
            }

            def list = []
            def topFiveCount = 0
            def String outColor 
            topFiveTasks.eachWithIndex { key, val, index ->
                if (index == 0){
                    outColor = "#3366cc"
                }
                if (index == 1){
                    outColor = "#ff9900"
                }
                if (index == 2){
                    outColor = "#109618"
                }
                def output = [name: key, value: val, color: outColor]
                topFiveCount = topFiveCount + val
                list << output
            }
            def otherCount = [name: "Others", value: totalClouds - topFiveCount, color: "#0099c6"]
            list << otherCount
            def json = JsonOutput.toJson(list)
            Map<String,Object> data = [totalClouds: totalClouds, awsClouds: awsClouds, openstackClouds: openstackClouds, standardClouds: standardClouds, taskOrder: maporder, cloudTable: outdemo, topFiveTasks: list, topFiveTasksJson: json ]
            ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_HEADER, displayOrder: displayOrder++, dataMap: data)
            morpheus.report.appendResultRows(reportResult,[resultRowRecord]).blockingGet()
        } // closes process  

// curly bracket here? 

     @Override
     String getDescription() {
         return "CSM Report Determine how a customer is using an environment."
     }

     @Override
     String getCategory() {
         return 'CSM'
     }

     @Override
     Boolean getOwnerOnly() {
         return false
     }

     @Override
     Boolean getMasterOnly() {
         return true
     }

     @Override
     Boolean getSupportsAllZoneTypes() {
         return true
     }
    // OPTION TO PUT IN LATER - CALENDAR TO ALLOW THEM TO SELECT A DATE RANGE.
     @Override
     List<OptionType> getOptionTypes() {
         []
         // [new OptionType(code: 'name', name: 'Search', fieldName: 'phrase', fieldContext: 'config', fieldLabel: 'Name Filter',     helpText: 'If no name supplied, this will search all.', displayOrder: 1),
         //new OptionType(code: 'site', name: 'Search', fieldName: 'group', fieldContext: 'config', fieldLabel: 'Group ID Filter',     helpText: 'If no group ID is supplied, this will search all.', displayOrder: 0)]
     }
    
}
