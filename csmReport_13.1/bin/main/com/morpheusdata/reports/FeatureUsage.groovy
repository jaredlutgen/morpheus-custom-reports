package com.morpheusdata.reports

import com.morpheusdata.core.AbstractReportProvider
import com.morpheusdata.core.MorpheusContext
import com.morpheusdata.core.Plugin
import com.morpheusdata.model.OptionType
import com.morpheusdata.model.ReportResult
import com.morpheusdata.model.ReportType
import com.morpheusdata.model.ReportResultRow
import com.morpheusdata.model.ContentSecurityPolicy
import com.morpheusdata.views.HTMLResponse
import com.morpheusdata.views.ViewModel
import com.morpheusdata.response.ServiceResponse
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import io.reactivex.Observable
import java.time.LocalDate
import java.time.Period

import java.sql.Connection

/**
 * A set of reports for CSMs to determine more about their customers. .
 * The goal of this report is to help understand how a customer is using the morpheus platform.
 *
 * A renderer is also defined to render the HTML via Handlebars templates.
 *
 * @author Peter Georgopoulos and Jared Lutgen
 */
 @Slf4j
class FeatureUsage extends AbstractReportProvider {
    Plugin plugin
    MorpheusContext morpheusContext

    FeatureUsage(Plugin plugin, MorpheusContext context) {
        this.plugin = plugin
        this.morpheusContext = context
    }

    @Override
    MorpheusContext getMorpheus() {
        morpheusContext
    }

    @Override
    Plugin getPlugin() {
        plugin
    }

    @Override
    String getCode() {
        'csm-featureUsage'
    }

    @Override
    String getName() {
        'Feature Usage'
    }

    ServiceResponse validateOptions(Map opts) {
         return ServiceResponse.success()
    }

    /*
     * Demonstrates building a TaskConfig to get details about the Instance and renders the html from the specified template.
     * @param instance details of an Instance
     * @return
     */
    @Override
    HTMLResponse renderTemplate(ReportResult reportResult, Map<String, List<ReportResultRow>> reportRowsBySection) {
        ViewModel<String> model = new ViewModel<String>()
        model.object = reportRowsBySection
        getRenderer().renderTemplate("hbs/featureUsage", model)
    }

    /**
     * Allows various sources used in the template to be loaded
     * @return
     */
    @Override
    ContentSecurityPolicy getContentSecurityPolicy() {
        def csp = new ContentSecurityPolicy()
        csp.scriptSrc = '*.jsdelivr.net'
        csp.frameSrc = '*.digitalocean.com'
        csp.imgSrc = '*.wikimedia.org'
        csp.styleSrc = 'https: *.bootstrapcdn.com'
        csp
    }


    void process(ReportResult reportResult) {
        morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.generating).blockingGet();
        Long displayOrder = 0
        List<GroovyRowResult> results = []
        withDbConnection { Connection dbConnection -> results = new Sql(dbConnection).rows("""SELECT
                                                                                                account.name as tenant,
                                                                                                (SELECT COUNT(instance.name) FROM instance WHERE instance.account_id = account.id) AS instanceCount,
                                                                                                (SELECT COUNT(policy.name) FROM policy WHERE policy.owner_id = account.id) AS policyCount,
                                                                                                (SELECT COUNT(task.name) FROM task WHERE task.account_id = account.id) AS taskCount,
                                                                                                (SELECT COUNT(task_set.name) FROM task_set WHERE task_set.account_id = account.id) AS workflowCount,
                                                                                                (SELECT COUNT(compute_site.name) FROM compute_site WHERE compute_site.account_id = account.id) AS groupCount,
                                                                                                (SELECT COUNT(compute_zone.name) FROM compute_zone WHERE compute_zone.account_id = account.id) AS cloudCount,
                                                                                                (SELECT count(instance.name) from instance where instance.account_id = account.id) as instanceCount,
                                                                                                (select count(app.name) from app where app.account_id = account.id) as appCount,
                                                                                                (select count(cypher_item.item_value) from cypher_item) as cypherCount,
                                                                                                (select count(option_type.name) from option_type where option_type.account_id = account.id and field_context="config.customOptions") as optionCount,
                                                                                                (select count(option_type_list.name) from option_type_list where option_type_list.account_id = account.id) as optionListCount,
                                                                                                (select count(account_integration.name) from account_integration where account_integration.account_id = account.id) as integrationCount
                                                                                            FROM
                                                                                                account
                                                                                            GROUP BY
                                                                                                account.id""")}
        log.info("Results: ${results}")
        Observable<GroovyRowResult> observable = Observable.fromIterable(results) as Observable<GroovyRowResult>
        observable.map{ resultRow ->
            // String uniqueCloudTypes = resultRow.uniqueCloudTypes
            log.info("Mapping resultRow ${resultRow}")
            Map<String,Object> data = [tenant: resultRow.tenant, instanceCount: resultRow.instanceCount, policyCount: resultRow.policyCount, taskCount: resultRow.taskCount, workflowCount: resultRow.workflowCount, groupCount: resultRow.groupCount, cloudCount: resultRow.cloudCount, appCount: resultRow.appCount, cypherCount: resultRow.cypherCount, optionCount: resultRow.optionCount, optionListCount: resultRow.optionListCount, integrationCount: resultRow.integrationCount  ]
            ReportResultRow resultRowRecord = new ReportResultRow(section: ReportResultRow.SECTION_MAIN, displayOrder: displayOrder++, dataMap: data)
            log.info("resultRowRecord: ${resultRowRecord.dump()}")
            return resultRowRecord
        }.buffer(50).doOnComplete {
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.ready).blockingGet();
        }.doOnError { Throwable t ->
            morpheus.report.updateReportResultStatus(reportResult,ReportResult.Status.failed).blockingGet();
        }.subscribe {resultRows ->
            morpheus.report.appendResultRows(reportResult,resultRows).blockingGet()
        }

    }

     @Override
     String getDescription() {
         return "Usage Stats on different Morpheus Features"
     }

     @Override
     String getCategory() {
         return 'Morpheus Customer Success'
     }

     @Override
     Boolean getOwnerOnly() {
         return false
     }

     @Override
     Boolean getMasterOnly() {
         return true
     }

     @Override
     Boolean getSupportsAllZoneTypes() {
         return true
     }
    // OPTION TO PUT IN LATER - CALENDAR TO ALLOW THEM TO SELECT A DATE RANGE.
     @Override
     List<OptionType> getOptionTypes() {
         []
         // [new OptionType(code: 'name', name: 'Search', fieldName: 'phrase', fieldContext: 'config', fieldLabel: 'Name Filter',     helpText: 'If no name supplied, this will search all.', displayOrder: 1),
         //new OptionType(code: 'site', name: 'Search', fieldName: 'group', fieldContext: 'config', fieldLabel: 'Group ID Filter',     helpText: 'If no group ID is supplied, this will search all.', displayOrder: 0)]
     }
    
}
